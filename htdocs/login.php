
			<div class="row main">
				<div class="main-login main-center">

		<form  method="post" action="includes/login.inc.php">
						<div class="form-group">
							<label for="username" class="cols-sm-2 control-label">Felhasználónév</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="username" id="username_id"  placeholder="Felhasználónév" required/>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Jelszó</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="pwd" id="pwd_id"  placeholder="Jelszó" required/>
								</div>
							</div>
						</div>

						<div class="form-group ">
							<input type="submit" name="log_button" id="log_button_id" class="btn  btn-lg btn-block login-button" value="Bejelentkezés"/>
						</div>

					</form>
	    </div>
		</div>
