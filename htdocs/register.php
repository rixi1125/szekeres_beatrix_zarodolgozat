
			<div class="row main">
				<div class="main-login main-center">
		<form  method="post" action="includes/register.inc.php" id="register_form">
						<div class="form-group">
							<label for="first_id" >Vezetéknév</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control " name="first" id="first_id"  placeholder="Vezetéknév" onChange="firstname_check(this.value,'firstnameerr')"  required/>
								</div>
								<div class="redcolor" id="firstnameerr"></div>
							</div>
						</div>

						<div class="form-group">
							<label for="last_id" >Keresztnév</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control " name="last" id="last_id"  placeholder="Keresztnév" onChange="lastname_check(this.value,'lastnameerr')"  required/>
								</div>
								<div class="redcolor" id="lastnameerr"></div>
							</div>
						</div>

						<div class="form-group">
							<label for="email_id" >E-mail cím</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
									<input type="email" class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" onChange="email_check(this.value,'mailerr')"   name="email" id="email_id"  placeholder="E-mail cím" required/>
								</div>
								<div class="redcolor" id="mailerr"></div>
							</div>
						</div>

						<div class="form-group">
							<label for="username_id" >Felhasználónév</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" pattern="^[a-zA-Z][a-zA-Z0-9-_\.]{4,20}$" name="username" id="username_id"  placeholder="Felhasználónév" onChange="username_check(this.value,'usernameerr')" required/>
								</div>
								<div class="redcolor" id="usernameerr"></div>
							</div>
						</div>

						<div class="form-group">
							<label for="pwd_id">Jelszó</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="pwd" id="pwd_id" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" placeholder="Jelszó" onChange="passw_check(this.value,'passwerr')" required/>
								</div>
								<div class="redcolor" id="passwerr"></div>
							</div>
						</div>

						<div class="form-group">
							<label for="pwd_id">Jelszó megerősítése</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="cpwd" id="cpwd_id"   placeholder="Jelszó megerősítése" onChange="passwagain_check(this.value,'passwagainerr')"  required/>
								</div>
								<div class="redcolor" id="passwagainerr"></div>
							</div>
						</div>



						<div class="form-group ">
							<input type="submit" name="reg_button" onclick="register()" id="reg_button_id" class="btn  btn-lg btn-block login-button" value="Regisztráció"/>
						</div>

			</form>
	  </div>
  </div>  
