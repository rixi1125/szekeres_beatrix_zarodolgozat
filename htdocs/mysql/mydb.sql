-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2018. Ápr 04. 19:43
-- Kiszolgáló verziója: 10.1.28-MariaDB
-- PHP verzió: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `mydb`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `jelentkezes`
--

CREATE TABLE `jelentkezes` (
  `Profil_azon` int(11) NOT NULL,
  `Naptar_azon` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `jelentkezes`
--

INSERT INTO `jelentkezes` (`Profil_azon`, `Naptar_azon`) VALUES
(2, 2),
(4, 4),
(4, 2);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `naptar`
--

CREATE TABLE `naptar` (
  `azon` int(11) NOT NULL,
  `idopont` datetime NOT NULL,
  `helyszin` varchar(255) CHARACTER SET utf8 NOT NULL,
  `leiras` varchar(255) CHARACTER SET utf8 NOT NULL,
  `pont` int(11) NOT NULL,
  `limitalt` int(11) NOT NULL,
  `Szervezo_azon` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `naptar`
--

INSERT INTO `naptar` (`azon`, `idopont`, `helyszin`, `leiras`, `pont`, `limitalt`, `Szervezo_azon`) VALUES
(1, '2018-04-11 09:00:00', 'Millenáris Park', 'Szemétszedés', 10, 15, 3),
(2, '2018-05-10 12:15:00', 'Városliget', 'Környezetvédő futás. Támogasd a környezetet, a növényeket a nevezési díjjal, és részvételeddel.', 25, 2, 2),
(3, '2018-03-13 10:30:00', 'Fővárosi Állatkert', 'Takarítás, szemétszedés. Az ottani dolgozók munkájának segítése. Közben ismerkedés az állatokkal.', 30, 2, 1),
(4, '2018-04-24 17:00:00', 'Budapest', 'Budapesten egyes játszóterek rendberakása.', 10, 10, 5);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `profil`
--

CREATE TABLE `profil` (
  `azon` int(11) NOT NULL,
  `veznev` varchar(30) CHARACTER SET utf8 NOT NULL,
  `kernev` varchar(30) CHARACTER SET utf8 NOT NULL,
  `email` varchar(35) CHARACTER SET utf8 NOT NULL,
  `felhnev` varchar(16) CHARACTER SET utf8 NOT NULL,
  `jelszo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `aktpont` int(11) NOT NULL,
  `datum` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `profil`
--

INSERT INTO `profil` (`azon`, `veznev`, `kernev`, `email`, `felhnev`, `jelszo`, `aktpont`, `datum`) VALUES
(1, 'Szekeres', 'Beatrix', 'szekeresbeatrix96@gmail.com', 'admin', '$2y$10$ZpDvMjzz6ZfFtcMe.vGoyOCCmtxdfbCgKriZajlIdyoadIYTTkgRK', 0, '2018-04-04 17:09:47.043332'),
(2, 'Kovács', 'Géza', 'kgeza@citromail.hu', 'kgeza', '$2y$10$RlRn85G8VaNa013XyHvygOs/UmdMuivHc7vMeC6CxJlaa.q/yq3Jm', 0, '2018-04-04 17:11:00.971428'),
(3, 'Kiss', 'Tímea', 'k.timea@gmail.com', 'kisstimi', '$2y$10$V39fJThVZLUO3ZiZxYO9kerCCIRhPQHLMroZE59hqXNw.m/PX8cNe', 0, '2018-04-04 17:11:27.709022'),
(4, 'Farkas', 'Nikolett', 'nikolett.farkas@outlook.hu', 'fniki99', '$2y$10$ZV1qbkl2cZBKcy8m/jiBwedQS84K/ZlaGCp1nlcKSUYsNSQWQN4jq', 0, '2018-04-04 17:12:01.957470'),
(5, 'Novák', 'Antal', 'kavon.antal@gmail.com', 'nova007', '$2y$10$fsrHajtOsvirxHTnN1MbSefIHE5qA3x6xAVHAAeFnOfOusLU.8jLi', 0, '2018-04-04 17:12:52.364611'),
(6, 'Lovász', 'Éva', 'l.eva@citromail.hu', 'evike11', '$2y$10$HXMscmgOffnhTEaKd/BkJeu1AzUI4AlYIh6eOKCjJZwKoHTCtjPkK', 0, '2018-04-04 17:16:52.754371'),
(7, 'Nagy', 'Áron', 'aron95@gmail.com', 'aron95', '$2y$10$8lpvQ90vWLS0VoPx.yboW.HYIR.H.eXb/lH7ohck/wx4GszNHwXEC', 0, '2018-04-04 17:17:26.041602');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `szervezo`
--

CREATE TABLE `szervezo` (
  `azon` int(11) NOT NULL,
  `nev` varchar(100) CHARACTER SET utf8 NOT NULL,
  `szekhely` varchar(255) CHARACTER SET utf8 NOT NULL,
  `telszam` varchar(12) CHARACTER SET utf8 NOT NULL,
  `email` varchar(35) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `szervezo`
--

INSERT INTO `szervezo` (`azon`, `nev`, `szekhely`, `telszam`, `email`) VALUES
(1, 'WWF', 'H-1141 Budapest, Álmos vezér útja 69/A', '0612145554', 'panda@wwf.hu'),
(2, 'Greenpeace', '1143 Budapest,\r\nZászlós utca 54.', '0613927663', 'tamogatas.hu@greenpeace.org'),
(3, 'Csalán Környezet- és Természetvédő Egyesület', '8200 Veszprém, Budapest u. 8.', '06309460658', 'titkarsag@csalan.hu'),
(4, 'Holocén Természetvédelmi Egyesület', '3525 Miskolc, Kossuth u. 13', '0646508944', 'holocen@holocen.hu'),
(5, 'Magyar Természetvédők Szövetsége', '1091 Budapest, Üllői út 91/b.', '0612167297', 'info@mtvsz.hu'),
(6, 'Zöld Kapcsolat Egyesület', '3525 Miskolc, Kossuth u. 13', '0646382095', 'info@zoldkapcsolat.hu'),
(7, 'Ökotárs Alapítvány', '1056 Budapest, Szerb utca 17–19.', ' 0614113500', 'info@okotars.hu'),
(8, 'Földkelte Kulturális és Környezetvédelmi Egyesület', '4600 Kisvárda, Toldi Miklós utca 29.', '06707759282', 'info@planetrise.org');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `jelentkezes`
--
ALTER TABLE `jelentkezes`
  ADD KEY `fk_Jelentkezes_Profil1_idx` (`Profil_azon`),
  ADD KEY `fk_Jelentkezes_Naptar1_idx` (`Naptar_azon`);

--
-- A tábla indexei `naptar`
--
ALTER TABLE `naptar`
  ADD PRIMARY KEY (`azon`),
  ADD KEY `fk_Naptar_Szervezo1_idx` (`Szervezo_azon`);

--
-- A tábla indexei `profil`
--
ALTER TABLE `profil`
  ADD PRIMARY KEY (`azon`);

--
-- A tábla indexei `szervezo`
--
ALTER TABLE `szervezo`
  ADD PRIMARY KEY (`azon`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `naptar`
--
ALTER TABLE `naptar`
  MODIFY `azon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT a táblához `profil`
--
ALTER TABLE `profil`
  MODIFY `azon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT a táblához `szervezo`
--
ALTER TABLE `szervezo`
  MODIFY `azon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `jelentkezes`
--
ALTER TABLE `jelentkezes`
  ADD CONSTRAINT `fk_Jelentkezes_Naptar1` FOREIGN KEY (`Naptar_azon`) REFERENCES `naptar` (`azon`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Jelentkezes_Profil1` FOREIGN KEY (`Profil_azon`) REFERENCES `profil` (`azon`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Megkötések a táblához `naptar`
--
ALTER TABLE `naptar`
  ADD CONSTRAINT `fk_Naptar_Szervezo1` FOREIGN KEY (`Szervezo_azon`) REFERENCES `szervezo` (`azon`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
