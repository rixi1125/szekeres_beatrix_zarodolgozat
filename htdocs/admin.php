<?php
include_once 'includes/dbh.inc.php';
if(!isset($_SESSION['user']['felhnev']) == 'admin')
{
	echo "<p style='color:green'>JELENTKEZZ BE!</p>";
}
else{
	$sql = "SELECT * FROM profil WHERE felhnev NOT LIKE 'admin'";
	$result = $conn->query($sql);
	$felhasznalok = [];
	if($result->num_rows > 0)
	{
	  while($data = $result->fetch_assoc()){
	       $felhasznalok[] = $data;
	  }
	}
	if(isset($_POST['bekuld'])){
		$ponthozzaad = mysqli_real_escape_string($conn,$_POST['pont_add']);
		$user_id = mysqli_real_escape_string($conn,$_POST['user_id']);
		  $sql = "UPDATE profil  SET aktpont = aktpont + $ponthozzaad WHERE azon = $user_id";
		  $result = mysqli_query($conn, $sql);
	  if ($result === TRUE) {
       header("location: index.php?oldal=admin") ;

	  } else {
		echo "Error: " . $sql . "<br>" . $conn->error;
	}
	}
  ?>
  <div class="col-sm-12">
  <table class="table table-hover">
  <thead>
    <tr>
      <th>Teljesnév</th>
      <th>Felhasználónév</th>
      <th>E-mail cím</th>
      <th>Pontszám</th>
      <th>Regisztráció dátuma</th>
	   <th>Pont hozzáadás </th>
    </tr>
  </thead>
  <tbody>
	<?php  foreach($felhasznalok as $felhasznalo){ ?>
	<tr>
      <td><?php echo $felhasznalo['veznev']; ?> <?php echo $felhasznalo['kernev']; ?> </td>
      <td><?php echo $felhasznalo['felhnev']; ?></td>
      <td><?php echo $felhasznalo['email']; ?></td>
      <td><?php echo $felhasznalo['aktpont']; ?></td>
      <td><?php echo $felhasznalo['datum']; ?></td>
	  <form method="POST" action="">
	   <input type="hidden" name="user_id" value="<?php echo $felhasznalo['azon']; ?>" />
	  <td><input type="number" name="pont_add" value="0" max="100"/></td>
	   <td><input type="submit" name="bekuld"/></td>
	   </form>
    </tr>
    </tr>
	<?php } ?>
  </tbody>
</table>
</div>
<?php }  ?>
