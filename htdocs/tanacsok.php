	<div class="content">
<h2>Használj nem eldobható dolgokat</h2>
<p>Ez a pont persze egyszerűnek hangzik, ugyanakkor, ha a jobban a mélyére nézünk, rájövünk, hogy nem is olyan könnyű betartani. <br/>Mire gondolunk ez alatt? Például:</p>
<ul>
	<li>Kertipartiknál igyekezzünk minél kevesebb papír- vagy műanyagtányért illetve poharat használni.</li>
	<li>Használjunk textilzsebkendőt az eldobható papír zsebkendő helyett.</li>
	<li>Gyermekeinket öltöztessük mosható pelenkába.</li>
</ul>
<h2>Gyűjtsd szelektíven a hulladékot </h2>
<p>Ez szintén eléggé magától értetődőnek tűnik, ám Magyarországon nagyon kevés embernek van a házában egynél több szemetes, amiben külön gyűjti a műanyagot, fémhulladékot illetve a papírt. </p>
<h2>A veszélyes hulladékot vidd az erre szolgáló gyűjtőbe</h2>
<p>Rengeteg ember nem tudja, mit kezdjen az elromlott, elavult elektronikai eszközökkel, lejárt szavatosságú vegyszerekkel/vegyianyagokkal, lemerült elemekkel illetve a sütéskor elhasznált olajjal, ezért egyszerűen csak kidobják a többi szeméttel együtt. Pedig ez nincs rendjén! Ezeket mind a megfelelő átvételi pontokra kell szállítani, ahol megfelelő eljárásokkal megsemmisítik vagy újrahasznosítják őket levéve ezzel a terhet természetanyánk válláról. </p>
<ul>
	<li>Az elektronikai eszközeinktől <a href="http://www.mediamarkt.hu/hu/shop/e_hulladek.html">itt</a> tudsz megszabadulni. </li>
	<li>A használt növényi olajat <a href="http://jovoujratoltve.hu/">itt</a> tudod leadni.</li>
	<li>A vegyszerek illetve veszélyes hulladékok lerakásáról <a href="http://www.fkf.hu/portal/page/portal/fkfzrt/hulladekkez/szelektiv_gyujtes/hulladekudvar">itt</a> tájékozódhatsz. </li>
</ul>
<h2>Vegyél állatkísérlet-mentes termékeket</h2>
<p>Nagyon sok kozmetikumot a gyártói állatokon tesztelnek, míg más gyártók külön hangsúlyt fektetnek az állatkísérletek elkerülésére. Tegyünk az állatkísérletek ellen és vásároljunk állatkísérlet-mentes termékeket! Az állatkísétleteket végző illetve nem végző cégek illetve márkák neveiről az <a href="http://features.peta.org/cruelty-free-company-search/index.aspx">alábbi linken</a> tájékozódhatsz (Angol weboldal). </p>
<h2>Használj mosogatógépet</h2>
<p>Amellett, hogy a mosogatógép az életünket nagyban megkönnyíti rengeteg energiát és vizet spórolunk meg vele ezáltal kevésbé szennyezve környezetünket és megkímélve pénztárcánkat.</p>
<h2>Moss alacsony hőfokon</h2>
<p>Kevesen tudják, de a ruháknak sokszor elég, ha alacsony hőfokon mossák őket. Ezzel a trükkel nem csak, hogy a természetet óvjuk, hanem ugyanúgy a pénztárcánkat és a ruhákat is.
Emellett törekedjünk arra, hogy ne indítsunk el mosást fél adaggal, mindig pakoljuk tele a mosógépünket. Ez persze a mosogatógépre is érvényes. </p>
<h2>Használd a tömegközlekedést vagy járj biciklivel</h2>
<p>Világszerte egyre több ember ismeri fel, hogy a reggeli illetve esti dugókat könnyűszerrel elkerülheti, ha a fenti két módszer egyikét választja. Egyre több város szélén vannak már úgynevezett P&R azaz Park and Ride parkolók, amik lehetővé teszik, hogy a város szélén leparkoljuk autóinkat és a városba már tömegközlekedéssel hajtsunk be.</p>
<h2>Víztakarékos vízcsap, zuhanyrózsa, wc tartály, stb…</h2>
<p>Ez ugyancsak egy kevésbé elterjedt dolog, ám néhány ilyen beszerzése hatalmas segítség tud lenni a környezetünknek. Ezen felül a pénztárcánkat is vastagítja.</p>
<h2>Ne kapcsolgasd a lámpákat</h2>
<p>„Ha kijössz a szobából, kapcsold le a lámpát!” Szerintem mindannyiunknak ismerősen cseng ez a mondat. Ám a mai világban már egyáltalán nem biztos, hogy kevesebb energiát használunk el, ha mindig leoltjuk a villanyt. Főleg, ha sokszor megyünk vissza ugyanabba a helyiségbe. <p>
Ennek az oka rendkívül egyszerű. A mai energiatakarékos ízzók már más elven működnek, mint a régi wolframszálas testvéreik. Ez energiafelhasználás szempontjából azt jelenti, hogy a működésük elején (tehát, amikor felkapcsoljuk a villanyt) az új energiatakarékos ízzók sokkal több áramot fogyasztanak, mint a hagyományos társaik. Ez a mennyiség akkor redukálódik jelentősen, amikor már teljes fényerővel világítanak. Ebből következik, hogy ha egy szobát nagyon gyakran használunk, ne kapcsolgassuk a villanyt. Jobban járunk, ha úgy hagyjuk.</p>
<p>Vagy, ha el akarjuk kerülni, hogy a lakásunk fényárban ússzon egész este ám mégis óvni szeretnénk a természetet, vásároljunk drágább ám tartósabb LED ízzókat. Ezen villanykörték előnyei, hogy nem csak kevés energiát fogyasztanak, de nem is tartalmaznak nemes gázokat ezzel tovább óvva környezetünket.
</p>
	</div>
