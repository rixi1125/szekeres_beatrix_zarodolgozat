//regisztráció ellenőrzés

var ok_email=0, ok_fname=0, ok_lname=0,ok_username=0, ok_pass=0;

function rosszkarakterek(str)
{
	var x;
	var i;
	for(i=0; i<45;i++)//46-os lehet az első ascii karakter"-"
	{
		x=String.fromCharCode(i);
		if(str.indexOf(x)>-1)
		{
			return true;
		}
	}

	x=String.fromCharCode(47);
	if(str.indexOf(x)>-1)// /-jel nem lehet
		{
			return true;
		}

		for(i=58; i<95;i++)// számok után kisbetűkig kizárva
			{
				x=String.fromCharCode(i);

				if(i!=64 && str.indexOf(x)>-1)
				{
					return true;
				}
			}

		for(i=123; i<256;i++)// betűk után kizárva
			{
				x=String.fromCharCode(i);
				if(str.indexOf(x)>-1)
				{
					return true;
				}
			}
	return false;
}

function email_check(e_text,err_place)
{

	valtozo=document.getElementById("email_id").value;
	if(valtozo != ""){
	if ((valtozo.indexOf('@')!=valtozo.lastIndexOf('@') || valtozo.indexOf('@')==-1)
	||
	valtozo.indexOf('@')==0 || valtozo.indexOf('@')>valtozo.length-5
	||
	valtozo.indexOf('.')==-1 || (valtozo.length-valtozo.lastIndexOf('.')!=3 && valtozo.length-valtozo.lastIndexOf('.')!=4)
	||
rosszkarakterek(valtozo)
	)
	{
		document.getElementById(err_place).innerHTML="Helytelen formátum.";
    document.getElementById("email_id").style.borderColor= "#F00" ;
  document.getElementById("email_id").style.borderStyle= "solid" ;
		ok_email=0;
  }
	else
		{
		document.getElementById("email_id").style.borderColor= "#ccc" ;
	document.getElementById(err_place).innerHTML="";
			ok_email=1;
		}
	} else {
		document.getElementById("email_id").style.borderColor= "#ccc" ;
	document.getElementById(err_place).innerHTML="";
	}
}

function firstname_check(fn_text,err_place)
{
	valtozo=document.getElementById("first_id").value;
		if(valtozo != ""){
if(valtozo.length < 2)
		{
			document.getElementById(err_place).innerHTML="A megadott név túl rövid.";
			document.getElementById("first_id").style.borderColor= "#F00" ;
			document.getElementById("first_id").style.borderStyle= "solid" ;
			ok_fname=0;
		}
	else if ( nev_rosszkarakterek(valtozo))
		{
			document.getElementById(err_place).innerHTML="Helytelen formátum.";
    document.getElementById("first_id").style.borderColor= "#F00" ;
  document.getElementById("first_id").style.borderStyle= "solid" ;
		ok_fname=0;
		}

	else
		{
		document.getElementById("first_id").style.borderColor= "#ccc" ;
	document.getElementById(err_place).innerHTML="";
			ok_fname=1;
		}
	} else {
		document.getElementById("first_id").style.borderColor= "#ccc" ;
	document.getElementById(err_place).innerHTML="";
	}
}

function lastname_check(fn_text,err_place)
{
	valtozo=document.getElementById("last_id").value;
		if(valtozo != ""){
	if(valtozo.length < 2)
		{
			document.getElementById(err_place).innerHTML="A megadott név túl rövid.";
			document.getElementById("last_id").style.borderColor= "#F00" ;
			document.getElementById("last_id").style.borderStyle= "solid" ;
			ok_fname=0;
		}
	else if ( nev_rosszkarakterek(valtozo))
		{

			document.getElementById(err_place).innerHTML="Helytelen formátum.";
    document.getElementById("last_id").style.borderColor= "#F00" ;
  document.getElementById("last_id").style.borderStyle= "solid" ;
		ok_lname=0;
		}

	else
		{
		document.getElementById("last_id").style.borderColor= "#ccc" ;
	document.getElementById(err_place).innerHTML="";
			ok_lname=1;
		}
	} else {
		document.getElementById("last_id").style.borderColor= "#ccc" ;
	document.getElementById(err_place).innerHTML="";
	}
}

function nev_rosszkarakterek(str)
	{
		var x;
		var i;
		for(i=0; i<32;i++) //space előtt kizárva
		{
			x=String.fromCharCode(i);
			if(str.indexOf(x)>-1)
			{
				return true;
			}
		}

		for(i=33; i<45;i++)  // - jelig kizárva
		{
			x=String.fromCharCode(i);
			if(str.indexOf(x)>-1)
			{
				return true;
			}
		}

		for(i=46; i<65;i++)  //nagybetűkig kizárva
		{
			x=String.fromCharCode(i);
			if(str.indexOf(x)>-1)
			{
				return true;
			}
		}

		for(i=91; i<97;i++) //kisbetűkig kizárva
		{
			x=String.fromCharCode(i);
			if(str.indexOf(x)>-1)
			{
				return true;
			}
		}


		return false;
	}



function felhasznalonev_karakterek(str)
{
	var x;
	var i;
	for(i=0; i<48;i++)
	{
		x=String.fromCharCode(i);
		if(str.indexOf(x)>-1)
		{
			return true;
		}
	}

		for(i=58; i<97;i++)// számok után kisbetűkig kizárva
		{
			x=String.fromCharCode(i);

			if(i!=64 && str.indexOf(x)>-1)
				{
					return true;
				}
		}

	return false;
}

function username_check(un_text,err_place)
{
	valtozo=document.getElementById("username_id").value;
	if(valtozo != ""){
	if(valtozo.length < 5)
		{
			document.getElementById(err_place).innerHTML="Túl rövid felhasználónév.";
			document.getElementById("username_id").style.borderColor= "#F00" ;
			document.getElementById("username_id").style.borderStyle= "solid" ;
			ok_username=0;
  	}
	else if(felhasznalonev_karakterek(valtozo))
		{
			document.getElementById(err_place).innerHTML="Helytelen karakterhasználat.";
			document.getElementById("username_id").style.borderColor= "#F00" ;
			document.getElementById("username_id").style.borderStyle= "solid" ;
			ok_username=0;

		}
	else
	{
		document.getElementById("username_id").style.borderColor= "#ccc" ;
	document.getElementById(err_place).innerHTML="";
		ok_username=1;
		}
	}else {
	document.getElementById(err_place).innerHTML="";
		document.getElementById("username_id").style.borderColor= "#ccc" ;
		ok_username =0;
 }
}



function jelszo_rosszkarakterek(str)
{
	var x;
	var i;
	for(i=0; i<32;i++)//space lehet az első karakter
	{
		x=String.fromCharCode(i);
		if(str.indexOf(x)>-1)
		{
			return true;
		}
	}

		for(i=127; i<256;i++)// fura karakterek kizárva
	{
		x=String.fromCharCode(i);
		if(str.indexOf(x)>-1)
		{
			return true;
		}
	}
	return false;

}

function szamok(str)
{
	var x;
	var i;
	for(i=48; i<58;i++) //számok
	{
		x=String.fromCharCode(i);
		if(str.indexOf(x)>-1)
		{
			return false;
		}
	}
	return true;
}
function nagybetuk(str)
{
	var i;
	var x;
	for(i=65; i<91;i++) //nagybetűk
	{
		x=String.fromCharCode(i);
		if(str.indexOf(x)>-1)
		{
			return false;
		}
	}
	return true;
}

function kisbetuk(str)
{
	var i;
	var x;
	for(i=97; i<123;i++) //kisbetűk
	{
		x=String.fromCharCode(i);
		if(str.indexOf(x)>-1)
		{
			return false;
		}
	}
	return true;
}


function passw_check(passw_text,err_place)
{
	valtozo=document.getElementById("pwd_id").value;
	if(valtozo != ""){
	if (valtozo.length < 6 || jelszo_rosszkarakterek(valtozo) || szamok(valtozo) || nagybetuk(valtozo) || kisbetuk(valtozo) )
		{	document.getElementById(err_place).innerHTML="A jelszónak legalább 6 karakterből kell állnia, és tartalmaznia kell legalább egy számot, valamint legalább egy kis- és nagybetűt.";
			document.getElementById("pwd_id").style.borderColor= "#F00" ;
			document.getElementById("pwd_id").style.borderStyle= "solid" ;
		 ok_pass=0;
		}
	else
		{
			document.getElementById("pwd_id").style.borderColor= "#ccc" ;
			document.getElementById(err_place).innerHTML="";
			ok_pass=1;
		}
	} else {
		document.getElementById("pwd_id").style.borderColor= "#ccc" ;
		document.getElementById(err_place).innerHTML="";
	}
}

function passwagain_check(passwa_text,err_place)
{
	valtozo=document.getElementById("pwd_id").value;
	valtozo2=document.getElementById("cpwd_id").value;
	if(valtozo != ""){
	if (valtozo != valtozo2)
		{
			document.getElementById(err_place).innerHTML="Nem egyeznek a jelszavak.";
			document.getElementById("cpwd_id").style.borderColor= "#F00" ;
			document.getElementById("cpwd_id").style.borderStyle= "solid" ;
			ok_pass=0;
		}
	else
		{
			document.getElementById("cpwd_id").style.borderColor= "#ccc" ;
			document.getElementById(err_place).innerHTML="";
			ok_pass=1;
		}
	} else {
		document.getElementById("cpwd_id").style.borderColor= "#ccc" ;
		document.getElementById(err_place).innerHTML="";
	}
}

function register()
{
	if (ok_email==1 && ok_lname ==1 && ok_fname ==1 && ok_username ==1 && ok_pass ==1 )
		{
			document.getElementById("register_form").submit();

		}
}
