	<div class="content">
		<div class="col-sm-4">
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="item active">
        <img src="images/index/kep20.jpg" alt="Olasz utcácska" style="width:100%;" class="img-responsive img-thumbnail" >
      </div>
      <div class="item">
        <img src="images/index/kep26.jpg" alt="Olasz kávézó kinti asztalai" style="width:100%;" class="img-responsive img-thumbnail" >
      </div>
      <div class="item">
        <img src="images/index/homeimage.jpg" alt="Olasz szűk utca erkélyekkel, növényekkel" style="width:100%;" class="img-responsive img-thumbnail" >
      </div>
    </div>
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Előző</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Következő</span>
    </a>
  </div>
		</div>
		<div class="col-sm-8">
			<h3>Fontos számodra a környezettudatos, környezetkímélő életmód? Fontos a te és környezeted egészsége?  Meg szeretnéd változtatni az életmódod, hogy az a te és bolygónk hasznára is váljon? Ha a fenti kérdésekre igen a válasz és készen is állsz arra, hogy a változás útjára lépj, jó helyen jársz.</h3>
<p>De hogy tudunk neked ebben segíteni?</p>
<ol>
<li>Regisztrálj az oldalunkra, hogy elérhetővé váljanak számodra az oldalunk által nyújtott lehetőségek. A regisztráció e-mail címhez kötött, ingyenes és a továbbiakban is az marad.</li>
<li>Jelentkezz be és nyisd meg a naptár menüpontot, és válogass az ott felkínált programok közül.</li>
<li>Ha megtetszett valamelyik esemény, jelentkezz rá, légy ott és az esemény végével megkapod a neked járó pontokat.</li>
</ol>
<p>Ennek az oldalnak a legfőbb célja, hogy felhívja a figyelmet a saját és környezetünk egészségének a védelmére illetve a környezettudatos életmódra.  Ha szeretnéd támogatni ezen célunkat, regisztrálj!</p>
		</div>
	</div>
