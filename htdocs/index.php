<?php
session_start();
if(isset($_POST['logout'])){
unset($_SESSION['user']);
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="css/style.css" rel="stylesheet" type="text/css">
  <script src="js/function.js"></script>
<title>Természet nélkül nincs élet</title>
</head>

<body>
<div class="container">
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="Logó az alábbi szöveggel: Természet nélkül nincs élet. Segíts hát te is!" /></a>
				</div>
					<ul class="nav navbar-nav">
					  <li><a href="index.php?oldal=fooldal">Főoldal</a></li>
					  <li><a href="index.php?oldal=galeria">Galéria</a></li>
					  <li><a href="index.php?oldal=tanacsok">Tanácsok</a></li>
           <?php if(isset($_SESSION['user'])){ ?> <li class=""><a href="index.php?oldal=profil">Profil</a></li>
												<li><a href="index.php?oldal=naptar">Naptár</a></li>
		   <?php }  ?>
       <?php

       if(isset($_SESSION['user'])){ ?>
    		   <?php if($_SESSION['user']['felhnev'] == 'admin'){ ?> <li><a href="index.php?oldal=admin">Admin</a></li> <?php  } ?>
           <?php if($_SESSION['user']['felhnev'] == 'admin'){ ?> <li><a href="index.php?oldal=admin_teendo">Esemény felvétel</a></li>
		       <?php }  ?>
       <?php }  ?>
					</ul>
					<ul class="nav navbar-nav navbar-right">
          <?php if(!isset($_SESSION['user'])){ ?>
						<li><a href="index.php?oldal=register"><span class="glyphicon glyphicon-user"></span> Regisztráció</a></li>
						<li><a href="index.php?oldal=login"><span class="glyphicon glyphicon-log-in"></span> Bejelentkezés</a></li>
          <?php } else { ?>

            <form method="post">
            <li><span class="glyphicon glyphicon-log-out"></span><input type="submit" name="logout" value="Kijelentkezés" /></li>
          </form>
          <?php } ?>
    </ul>
			</div>
		</nav>


  <?php
    if(isset($_GET['oldal'])){
        if($_GET['oldal'] == 'galeria') {
        include("{$_GET['oldal']}.php");
      } elseif($_GET['oldal'] == 'tanacsok') {
        include("{$_GET['oldal']}.php");
      } elseif($_GET['oldal'] == 'register') {
        include("{$_GET['oldal']}.php");
      } elseif($_GET['oldal'] == 'login') {
        include("{$_GET['oldal']}.php");
      } elseif($_GET['oldal'] == 'profil') {
        include("{$_GET['oldal']}.php");
		} elseif($_GET['oldal'] == 'naptar') {
        include("{$_GET['oldal']}.php");
      } elseif($_GET['oldal'] == 'admin') {
        include("{$_GET['oldal']}.php");
      } elseif($_GET['oldal'] == 'admin_teendo') {
        include("{$_GET['oldal']}.php");
      }else {
        include("{$_GET['oldal']}.php");
      }
    } else {
        include("fooldal.php");
    }

  ?>

	<div id="footer">
	<div class="col-sm-4">
        <p class="title">Elérhetőségek</p>
        <table>
          <tbody>
            <tr>
              <td><i class="fa fa-map-marker"></i></td>
              <td>2072 Zsámbék, Petőfi Sándor u. 69.</td>
            </tr>
            <tr>
              <td><i class="fa fa-phone"></i></td>
              <td>Tel.: + 36 30 910 1539</td>
            </tr>
            <tr>
              <td><i class="fa fa-envelope"></i></td>
              <td><a href="mailto:szekeresbeatrix96@gmail.com"> szekeresbeatrix96@gmail.com</a></td>
            </tr>
          </tbody>
        </table>
		</div>
</div>
<div id="copyright">
	<div class="col-sm-4">
        <p>Szekeres Beatrix  © 2018 Minden Jog fenntartva. </p>
		</div>
</div>
	</div>
</body>
</html>
