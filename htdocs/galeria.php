<div class="content">
		<div class="row">
    <div class="col-sm-4">
      <div class="thumbnail">
        <a href="images/gallery/kep15.jpg" target="_blank">
          <img src="images/gallery/kep15.jpg" alt="öböl" style="width:100%">
          <div class="caption">
            <p>Olaszország, Puglia régió, Polignano</p>
          </div>
        </a>
      </div>
    </div>

     <div class="col-sm-4">
      <div class="thumbnail">
        <a href="images/gallery/kep24.jpg" target="_blank">
          <img src="images/gallery/kep24.jpg" alt="füves várárok pálmafával" style="width:100%">
          <div class="caption">
            <p>Olaszország, Puglia régió, Bari </p>
          </div>
        </a>
      </div>
    </div>

     <div class="col-sm-4">
      <div class="thumbnail">
        <a href="images/gallery/kep25.jpg" target="_blank">
          <img src="images/gallery/kep25.jpg" alt="Erzsébet-kilátóból a látvány: fák, erdő, Budapest látképe" style="width:100%">
          <div class="caption">
            <p>Magyarország, Budapest, Erzsébet-kilátó</p>
          </div>
        </a>
      </div>
    </div>
		</div>

    <div class="row">
     <div class="col-sm-4">
      <div class="thumbnail">
        <a href="images/gallery/kep27.jpg" target="_blank">
          <img src="images/gallery/kep27.jpg" alt="Lánc-híd" style="width:100%">
          <div class="caption">
            <p>Magyarország, Budapest, Lánc-híd</p>
          </div>
        </a>
      </div>
    </div>


     <div class="col-sm-4">
      <div class="thumbnail">
        <a href="images/gallery/kep29.jpg" target="_blank">
          <img src="images/gallery/kep29.jpg" alt="Margit-híd" style="width:100%">
          <div class="caption">
            <p>Magyarország, Budapest, Margit-híd</p>
          </div>
        </a>
      </div>
    </div>

    <div class="col-sm-4">
      <div class="thumbnail">
        <a href="images/gallery/kep18.jpg" target="_blank">
          <img src="images/gallery/kep18.jpg" alt="öböl" style="width:100%">
          <div class="caption">
            <p>Olaszország, Puglia régió, Polignano</p>
          </div>
        </a>
      </div>
    </div>
    </div>

    <div class="row">
    <div class="col-sm-4">
      <div class="thumbnail">
        <a href="images/gallery/kep22.jpg" target="_blank">
          <img src="images/gallery/kep22.jpg" alt="szökőkút" style="width:100%">
          <div class="caption">
            <p>Olaszország, Puglia régió, Alberobello</p>
          </div>
        </a>
      </div>
    </div>

     <div class="col-sm-4">
      <div class="thumbnail">
        <a href="images/gallery/kep30.jpg" target="_blank">
          <img src="images/gallery/kep30.jpg" alt="budapesti parlament" style="width:100%">
          <div class="caption">
            <p>Magyarország, Budapest, Parlament</p>
          </div>
        </a>
      </div>
    </div>

     <div class="col-sm-4">
      <div class="thumbnail">
        <a href="images/gallery/kep35.jpg" target="_blank">
          <img src="images/gallery/kep35.jpg" alt="éttermi asztalok tengerre néző kilátással Bulgáriában" style="width:100%">
          <div class="caption">
            <p>Bulgária, Napospart, Nesebar</p>
          </div>
        </a>
      </div>
    </div>
    </div>

     <div class="row">
    <div class="col-sm-4">
      <div class="thumbnail">
        <a href="images/gallery/kep37.JPG" target="_blank">
          <img src="images/gallery/kep37.jpg" alt="Mátyás király szobra Kolozsváron" style="width:100%">
          <div class="caption">
            <p>Románia, Erdély, Kolozsvár</p>
          </div>
        </a>
      </div>
    </div>

     <div class="col-sm-4">
      <div class="thumbnail">
        <a href="images/gallery/kep41.jpg" target="_blank">
          <img src="images/gallery/kep41.jpg" alt="Tordai hasadék Erdélyben" style="width:100%">
          <div class="caption">
            <p>Románia, Erdély, Tordai-hasadék</p>
          </div>
        </a>
      </div>
    </div>

     <div class="col-sm-4">
      <div class="thumbnail">
        <a href="images/gallery/kep46.jpg" target="_blank">
          <img src="images/gallery/kep46.jpg" alt="erdélyi fás látkép" style="width:100%">
          <div class="caption">
            <p>Románia, Erdély</p>
          </div>
        </a>
      </div>
    </div>
    </div>

     <div class="row">
    <div class="col-sm-4">
      <div class="thumbnail">
        <a href="images/gallery/kep52.jpg" target="_blank">
          <img src="images/gallery/kep52.jpg" alt="Budapesti parlament képe éjszaka a Margit-hídról" style="width:100%">
          <div class="caption">
            <p>Magyarország, Budapest, Parlament</p>
          </div>
        </a>
      </div>
    </div>

     <div class="col-sm-4">
      <div class="thumbnail">
        <a href="images/gallery/kep58.jpg" target="_blank">
          <img src="images/gallery/kep58.jpg" alt="A London Eyeról a látkép belülről fotózva" style="width:100%">
          <div class="caption">
            <p>Anglia, London, The London Eye</p>
          </div>
        </a>
      </div>
    </div>

     <div class="col-sm-4">
      <div class="thumbnail">
        <a href="images/gallery/kep62.jpg" target="_blank">
          <img src="images/gallery/kep62.jpg" alt="Tower Bridge" style="width:100%">
          <div class="caption">
            <p>Anglia, London, Tower Bridge</p>
          </div>
        </a>
      </div>
    </div>
    </div>
 </div>
