<?php
if (isset($_POST['reg_button']))
{

	include_once 'dbh.inc.php';
	$first = mysqli_real_escape_string($conn,$_POST['first']);
	$last = mysqli_real_escape_string($conn,$_POST['last']);
	$email = mysqli_real_escape_string($conn,$_POST['email']);
	$username = mysqli_real_escape_string($conn,$_POST['username']);
	$pwd = mysqli_real_escape_string($conn,$_POST['pwd']);
	$cpwd = mysqli_real_escape_string($conn,$_POST['cpwd']);
	// hibakezelők
	// üres mezőket ellenőrzi
	if(empty($first) || empty($last) || empty($email)|| empty($username) || empty($pwd) || empty($cpwd)){
		echo "Üresen hagytál egy vagy több mezőt.";
	}
	else{
				//email helyes-e
				if(!filter_var($email, FILTER_VALIDATE_EMAIL))
				{
					echo "Helytelen a megadott e-mail cím.";
				}
				else{
					$sql ="SELECT * from profil WHERE felhnev ='$username'";
					$result = mysqli_query($conn, $sql);
						$resultCheck = mysqli_num_rows($result); // ha van találat akkor hibaüzenetet küldünk

					if($resultCheck > 0)
					{
						echo "A $username felhasználónév már foglalt. Kérlek válassz másikat.";

					}	else{
						//jelszó hashelése
					  	$hashedPwd = password_hash($pwd,PASSWORD_DEFAULT);
						//felhasználó beillesztése az adatbázisba
						$sql = "INSERT INTO profil(veznev, kernev,email,felhnev,jelszo) VALUES ('$first', '$last','$email','$username','$hashedPwd')";
						$result = mysqli_query($conn, $sql);

						echo "Sikeres regisztráció. Most már be tudsz jelentkezni.";
					}
				}
			}
	} else
{
	header("location:../index.php?oldal=register");   //megnyomták-e egyáltalán a gombot, ha nem akkor visszamennek az oldalra
}
?>
