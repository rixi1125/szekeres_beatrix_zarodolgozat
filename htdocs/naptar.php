<?php
if(!isset($_SESSION['user'])){
  echo "<p style='color:green'>JELENTKEZZ BE!</p>";
} else {
  ?>
<?php
 include_once 'includes/dbh.inc.php';
 if(isset($_POST['delevent'])){
   if(date("Y-m-d") > $_POST['esemeny_idopont']){
     $sql = "DELETE FROM naptar WHERE azon = '".$_POST["esemeny_azon"]."'";
	 $result = $conn->query($sql);
   if($result){
    echo "<p style='color:red;margin-left:1%;'>Esemény törölve!</p>";
  } else {
    echo "ERROR". $conn->error;
  }
} else {
  echo "<p style='color:red;margin-left:1%;'>Eseményt nem lehet törölni, még nem ért véget!</p>";
}
 }


 if(isset($_POST['rendez'])){
   if($_POST['variaco'] == 'ORDER BY idopont ASC'){
      	$sql = "SELECT idopont,helyszin,leiras,pont,limitalt,szervezo_azon,naptar.azon as 'naptar_azon',Szervezo.azon FROM naptar,Szervezo  WHERE Szervezo_azon = Szervezo.azon ".$_POST['variaco']." ";
        $result = $conn->query($sql);
        $esemenyek = [];
        if($result->num_rows > 0)
        {
          while($data = $result->fetch_assoc()){
               $esemenyek[] = $data;
          }
        }
  } else if($_POST['variaco'] == 'ORDER BY helyszin ASC'){
       $sql = "SELECT idopont,helyszin,leiras,pont,limitalt,szervezo_azon,naptar.azon as 'naptar_azon',Szervezo.azon FROM naptar,Szervezo  WHERE Szervezo_azon = Szervezo.azon ".$_POST['variaco']." ";
       $result = $conn->query($sql);
       $esemenyek = [];
       if($result->num_rows > 0)
       {
         while($data = $result->fetch_assoc()){
              $esemenyek[] = $data;
         }
       }
}
} else {
	$sql = "SELECT idopont,helyszin,leiras,pont,limitalt,szervezo_azon,naptar.azon as 'naptar_azon',Szervezo.azon FROM naptar,Szervezo  WHERE Szervezo_azon = Szervezo.azon ";
  $result = $conn->query($sql);
	$esemenyek = [];
	if($result->num_rows > 0)
	{
	  while($data = $result->fetch_assoc()){
	       $esemenyek[] = $data;
	  }
	}
}


  if(isset($_POST['jelentkezes'])){
    $uzenet = "";
    $sql = "SELECT COUNT(*) as 'jelentkezok', limitalt FROM jelentkezes,naptar WHERE  azon = '".$_POST["esemeny_azon"]."' AND naptar_azon = naptar.azon";
    $result = $conn->query($sql);
    if($result->num_rows > 0){
      while($data = $result->fetch_assoc()){
  	       if($data['jelentkezok'] == $data['limitalt']){
             $uzenet = "Már betelt a hely!";
             echo "<p style='color:red;margin-left:1%;'>".$uzenet."</p>";
           }
  	  }
    }
    if($uzenet == ""){	
	$sql="SELECT * FROM jelentkezes WHERE naptar_azon = '".$_POST["esemeny_azon"]."' AND profil_azon = '".$_SESSION['user']['azon']."'" ;
	$result = $conn->query($sql);
    if($result->num_rows == 1){
		echo "<p style='color:red;margin-left:1%;'>Erre az eseményre már jelentkeztél.</p>";
	}
	else {
      $sql = "INSERT INTO jelentkezes (profil_azon, naptar_azon) VALUES (".$_SESSION['user']['azon'].",'".$_POST['esemeny_azon']."')";
      $result = $conn->query($sql);
      if($result){
         echo "<p style='color:green;margin-left:1%;'>Sikeresen jelentkeztél.</p>";
      }else {
         echo $conn->error;
         //echo $sql;
       }
	}
   }
 }

  ?>
  <div id="order">
  <form method="POST" action="">
    <select name="variaco">
       <option value="ORDER BY idopont ASC">Időpont szerint</option>
       <option value="ORDER BY helyszin ASC">ABC szerint</option>
    </select>
   <td><input type="submit" name="rendez" value="Rendezés"/></td>
   </form>
   </div>
   <div class="col-sm-12">
<table class="table table-hover">
  <thead>
    <tr>
      <th>Időpont</th>
      <th>Helyszín</th>
      <th>Leírás</th>
      <th>Pontszám</th>
      <th>Max résztvevők száma</th>
    </tr>
  </thead>
  <tbody>
	<?php  foreach($esemenyek as $esemeny){ ?>
	<tr>

      <td><?php echo $esemeny['idopont']; ?> </td>
      <td><?php echo $esemeny['helyszin']; ?></td>
      <td><?php echo $esemeny['leiras']; ?></td>
      <td><?php echo $esemeny['pont']; ?></td>
     <td><?php echo $esemeny['limitalt']; ?></td>

	  <form method="POST" action="">
	    <input type="hidden" name="szervezo_azon" value="<?php echo $esemeny['szervezo_azon']; ?>" />
	   <input type="hidden" name="esemeny_azon" value="<?php echo $esemeny['naptar_azon']; ?>" />
      <td> <input type="submit" name="jelentkezes" value="Jelentkezés"/></td>
      <?php if($_SESSION['user']['felhnev'] == "admin"){ ?>
         <input type="hidden" name="esemeny_idopont" value="<?php echo $esemeny['idopont']; ?>" />
  	      <td> <input type="submit" name="delevent" value="Törlés"/></td>
    <?php } ?>
	   </form>
    </tr>
    </tr>
	<?php } ?>
  </tbody>
  </table>
  </div>
<?php } ?>
