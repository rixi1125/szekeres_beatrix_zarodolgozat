<?php
if(!isset($_SESSION['user'])){
  echo "<p style='color:green'>JELENTKEZZ BE!</p>";
} else {
  include_once 'includes/dbh.inc.php';
  	$sql = "SELECT azon,idopont,helyszin,leiras,pont,limitalt,szervezo_azon,naptar.azon as 'naptar_azon' FROM naptar,jelentkezes  WHERE naptar_azon = azon AND  profil_azon = '".$_SESSION['user']["azon"]."' ";
   $result = $conn->query($sql);
   $esemenyek = [];
   $hiba = "";
   if($result->num_rows > 0)
   {
     while($data = $result->fetch_assoc()){
          $esemenyek[] = $data;
     }
   } else {
     $hiba = "<p style='color:green;margin-left:1%;'>Nem jelentkeztél még semmire.</p>";
   }

   if(isset($_POST['delesemeny'])){
     $sql = "DELETE FROM jelentkezes  WHERE naptar_azon = '".$_POST["naptar_azon"]."' AND  profil_azon = '".$_SESSION['user']["azon"]."' ";
    $result = $conn->query($sql);
    if($result){
      echo "<p style='color:red;margin-left:1%;'>Jelentkezés törölve.</p>";
    }
   }

  ?>
<h2 style="margin-left:1%">Üdvözöllek, <?php echo $_SESSION['user']['kernev']; ?>!</h2>
<div class="col-sm-12">
<table class="table table-hover">
  <thead>
    <tr>
      <th>Teljesnév</th>
      <th>Felhasználónév</th>
      <th>E-mail cím</th>
      <th>Pontszám</th>
      <th>Regisztráció dátuma</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><?php echo $_SESSION['user']['veznev']; ?> <?php echo $_SESSION['user']['kernev']; ?> </td>
      <td><?php echo $_SESSION['user']['felhnev']; ?></td>
      <td><?php echo $_SESSION['user']['email']; ?></td>
      <td><?php echo $_SESSION['user']['aktpont']; ?></td>
      <td><?php echo $_SESSION['user']['datum']; ?></td>
      <form method="POST" action="includes/deleteaccount.php">
	  <td><input type="submit" name="delaccount" value="Felhasználói fiók törlése"/></td>
    </form>
    </tr>
  </tbody>
</table>
</div>

<h2 style="margin-left:1%">Események, amikre jelentkeztem: </h2></br>



<?php
if($hiba != ""){
  echo $hiba;
} else{
  ?>

<div class="col-sm-12">
<table class="table table-hover">
  <thead>
    <tr>
      <th>Időpont</th>
      <th>Helyszín</th>
      <th>Leírás</th>
      <th>Pontszám</th>
      <th>Max résztvevők száma</th>
    </tr>
  </thead>
  <tbody>
	<?php  foreach($esemenyek as $esemeny){ ?>
	<tr>

      <td><?php echo $esemeny['idopont']; ?> </td>
      <td><?php echo $esemeny['helyszin']; ?></td>
      <td><?php echo $esemeny['leiras']; ?></td>
      <td><?php echo $esemeny['pont']; ?></td>
     <td><?php echo $esemeny['limitalt']; ?></td>
     <form method="POST" action="">
    <td><input type="hidden" name="naptar_azon" value="<?php echo $esemeny['azon']; ?>"/></td>
    <td><input type="submit" name="delesemeny" value="Esemény törlése"/></td>
   </form>
    </tr>
    </tr>
	<?php } ?>
  </tbody>
  </table>
  </div>
  <?php } ?>
<?php } ?>
